package edu.com.asus.getdatawtihjsoup;

/**
 * Created by ASUS on 7/12/2017.
 */

public interface PlayTrailerCallBack {

    void onPreExecute() ;
    void onPostExecute(String s);
}
