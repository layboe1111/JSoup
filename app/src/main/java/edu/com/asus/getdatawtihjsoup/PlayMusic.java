package edu.com.asus.getdatawtihjsoup;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

public class PlayMusic extends AppCompatActivity implements PlayTrailerCallBack {
    Button btnPaly ;
    VideoView playTrailer;
    PlayTrailerParser trailerParser ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_music);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        btnPaly = (Button) findViewById(R.id.btnPlay);
        playTrailer = (VideoView) findViewById(R.id.PlayTrailer);
        new PlayTrailerParser(this).execute();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onPreExecute() {

    }

    @Override
    public void onPostExecute(String s) {

    }
}
