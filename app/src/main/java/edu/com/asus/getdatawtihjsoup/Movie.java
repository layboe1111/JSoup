package edu.com.asus.getdatawtihjsoup;

/**
 * Created by ASUS on 7/11/2017.
 */

public class Movie {
    private String Title;
    private String imgageUrl;
    private String LinkYoutube;
    private String Link ;

    public Movie(String title, String Postimg, String LinkYoutube, String link) {
        Title = title;
        imgageUrl = Postimg;
        LinkYoutube = LinkYoutube;
        Link = link;
    }

    public Movie(String title, String Postimg, String link) {
        Title = title;
        imgageUrl = Postimg;
        Link = link;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImgUrl() {
        return imgageUrl;
    }

    public void setImgUrl(String Postimg) {
        imgageUrl = Postimg;
    }

    public String getImageUrl() {
        return LinkYoutube;
    }

    public void setImageUrl(String LinkYoutube) {
        LinkYoutube = LinkYoutube;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }


}
