package edu.com.asus.getdatawtihjsoup;

import java.util.List;

/**
 * Created by ASUS on 7/11/2017.
 */

public interface CallbackMethod {
    void onPreExecute();
    void onPostExecute(List<Movie> movieList);
    void onError();
}
