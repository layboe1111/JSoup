package edu.com.asus.getdatawtihjsoup;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

/**
 * Created by ASUS on 7/12/2017.
 */

public class PlayTrailerParser extends AsyncTask<Void, Void, String> {
    private String vieourl;
    private PlayTrailerCallBack callback;
    private String trailerUrl;

    public PlayTrailerParser(PlayTrailerCallBack callback) {
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        callback.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {
        String baseUrl = "https://www.legend.com.kh";
        String url = baseUrl + "/Browsing/Movies/Details/h-0100000895";



        Document document = null;

        try {
            Log.e("oooo", url);
            document = Jsoup.connect(url).get();
            String movieUrl = "#trailer";

            System.out.print(document);

            Element element = document.select(movieUrl).first();
            Log.e("ooo", element.attr("href"));

//            Elements movieTrailer = document.select(movieUrl);
//            Log.e("oooo" , "element");
//            for (Element trailer : movieTrailer){
//                trailerUrl = trailer.select("div#trailer-wrapper a#trailer").get(0).attr("href");
//                Log.e("oooo" , "for Loop");
//                Log.e("pppp" , trailerUrl);
//            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return trailerUrl;
    }

    @Override
    protected void onPostExecute(String s) {
        callback.onPostExecute(s);
    }
}
