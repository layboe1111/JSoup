package edu.com.asus.getdatawtihjsoup;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 7/11/2017.
 */

public class CinimarParser extends AsyncTask<Void , Void , List<Movie>> {

    private List<Movie> movielist;
    private CallbackMethod callbackMethod;
    public CinimarParser(CallbackMethod callbackMethod){
        movielist = new ArrayList<>();
        this.callbackMethod = callbackMethod ;
    }

    @Override
    protected void onPreExecute() {
        callbackMethod.onPreExecute();
    }

    @Override
    protected List<Movie> doInBackground(Void... voids) {
        String baseUrl = "https://www.legend.com.kh";
        String url = baseUrl + "/Browsing/Movies/NowShowing";
        Document document = null ;

        try {
            document = Jsoup.connect(url).get();
            Elements movies = document.select("article#movies-list div.list-item.movie");
            for (Element movie: movies){
                String posterUrl = movie.select("div.image-outer img").first().attr("src");
                Log.e("pppp" , posterUrl);
                String title = movie.select("div.item-details h3.item-title").first().text();
                String link = baseUrl + movie.select("div.image-outer a").first().attr("href");

                Movie item = new Movie(title , posterUrl , link);
                movielist.add(item);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return movielist;
    }

    @Override
    protected void onPostExecute(List<Movie> movies) {
        callbackMethod.onPostExecute(movies);
    }
}
