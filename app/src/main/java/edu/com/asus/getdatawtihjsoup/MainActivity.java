package edu.com.asus.getdatawtihjsoup;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CallbackMethod {
    ProgressDialog dialog ;
    RecyclerView rvlist ;
    List<Movie> movieList ;
    private RecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        movieList = new ArrayList<>();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait...!");
        new CinimarParser(this).execute();
        rvlist = (RecyclerView) findViewById(R.id.rvlist);
        rvlist.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
        adapter = new RecyclerViewAdapter(movieList , this);
        rvlist.setAdapter(adapter);

    }

    @Override
    public void onPreExecute() {
        dialog.show();
    }

    @Override
    public void onPostExecute(List<Movie> movieList) {
        this.movieList.addAll(movieList);
        adapter.notifyDataSetChanged();
        dialog.dismiss();
    }

    @Override
    public void onError() {
        dialog.dismiss();
    }
}
